@echo off
docker-compose up -d user-db room-db reservation-db key-db
timeout 3
type @ihbs\user-service\create_database.sql | docker exec -i user-db psql -U postgres
type @ihbs\room-service\create_database.sql | docker exec -i room-db psql -U postgres
type @ihbs\reservation-service\create_database.sql | docker exec -i reservation-db psql -U postgres
type @ihbs\key-service\create_database.sql | docker exec -i key-db psql -U postgres
docker-compose down
