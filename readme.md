## Initial setup

```
.\init_databases
.\seed_databases
docker-compose up install
```

## Running

```
docker-compose up
```