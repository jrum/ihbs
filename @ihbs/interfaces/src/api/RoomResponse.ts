export interface RoomResponse {
    id: string;
    name: string;
    price: number;
}