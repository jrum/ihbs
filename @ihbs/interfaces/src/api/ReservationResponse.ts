export interface ReservationResponse {
    id: string;
    userId: string;
    roomId: string;
    starts: string;
    ends: string;
}