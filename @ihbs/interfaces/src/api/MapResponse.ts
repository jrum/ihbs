export interface MapResponse<T> {
    [id: string]: T
}