import { RoomResponse } from "./RoomResponse";

export interface KeyResponse {
    id: string;
    room: RoomResponse;
    code: string;
    validFrom: string;
    validUntil: string;
}