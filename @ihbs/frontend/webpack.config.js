const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/main.tsx",
    module: {
        rules: [
            {
                test: /\.(tsx?|js)$/,
                loader: "ts-loader",
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    resolve: {
        modules: [
            path.resolve(__dirname, "src"),
            "node_modules"
        ],
        extensions: [".ts", ".tsx", ".js"]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "src/index.html",
            inject: true
        })
    ],
    devServer: {
        compress: true,
        host: "0.0.0.0",
        port: 80,
        public: "localhost:10000",
        historyApiFallback: {
            disableDotRule: true
        },
        watchOptions: {
            poll: 800,
            aggregateTimeout: 200,
            ignored: /node_modules/
        },
    },
    devtool: "source-map",
    output: {
        filename: "[name].[chunkhash].js",
        path: path.resolve(__dirname, "dist"),
        publicPath: "/"
    }
};