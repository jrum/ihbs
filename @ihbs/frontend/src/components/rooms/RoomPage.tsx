import { RoomResponse } from "@ihbs/interfaces";
import { RoomCreateForm } from "components/rooms/RoomCreateForm";
import { RoomList } from "components/rooms/RoomList";
import * as React from "react";
import { get, post } from "util/http";

export const RoomPage: React.FC = () => {
    const [rooms, setRooms] = React.useState<RoomResponse[]>([]);
    const [fetching, setFetching] = React.useState(true);
    const [error, setError] = React.useState(false);

    React.useEffect(() => {
        async function fetchRooms() {
            try {
                setRooms(await get<RoomResponse[]>("/api/rooms"));
                setError(false);
            } catch {
                setError(true);
            } finally {
                setFetching(false);
            }
        }

        if (fetching) {
            fetchRooms();
        }
    }, [fetching]);

    async function createRoom(name: string, price: number) {
        await post("/api/rooms", { name, price });
        setFetching(true);
    }

    let content;
    if (fetching) {
        content = <p>Loading rooms...</p>;
    } else if (error) {
        content = <>
            <p>Failed to load rooms.</p>
            <button onClick={() => setFetching(true)}>Reload</button>
        </>;
    } else {
        content = <>
            <RoomList rooms={rooms} />
            <RoomCreateForm onSubmit={createRoom} />
        </>;
    }

    return <>
        <h1>Rooms</h1>
        {content}
    </>;
}