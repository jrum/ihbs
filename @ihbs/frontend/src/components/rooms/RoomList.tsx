import { RoomResponse } from "@ihbs/interfaces";
import * as React from "react";

interface Props {
    rooms: RoomResponse[];
}

export const RoomList: React.FC<Props> = props => {

    function renderRow(room: RoomResponse) {
        return <tr key={room.id}>
            <td>{room.name}</td>
            <td>{room.price}</td>
        </tr>;
    }

    return <table>
        <thead>
            <tr>
                <td>Room</td>
                <td>Hourly price</td>
            </tr>
        </thead>
        <tbody>
            {props.rooms.map(x => renderRow(x))}
        </tbody>
    </table>;
}
