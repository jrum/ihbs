import * as React from "react";

interface Props {
    onSubmit: (name: string, price: number) => void;
}

export const RoomCreateForm: React.FC<Props> = props => {
    const [name, setName] = React.useState("");
    const [price, setPrice] = React.useState(0);

    function handleSubmit(event: React.FormEvent) {
        event.preventDefault();
        props.onSubmit(name, price);
    }

    function handleNameChange(event: React.ChangeEvent<HTMLInputElement>) {
        setName(event.target.value);
    }

    function handlePriceChange(event: React.ChangeEvent<HTMLInputElement>) {
        const number = Number(event.target.value);
        if (!isNaN(number)) {
            setPrice(number);
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <input type="text" value={name} placeholder="Name" required onChange={handleNameChange}/>
            <input type="number" defaultValue="" step="any" placeholder="Price" required onChange={handlePriceChange}/>
            <input type="submit" value="Create" />
        </form>
    );
}
