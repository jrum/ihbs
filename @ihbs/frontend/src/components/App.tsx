import * as React from "react";
import { UserPage } from "components/users/UserPage";
import { RoomPage } from "components/rooms/RoomPage";
import { ReservationPage } from "components/reservations/ReservationPage";

export const App: React.FC = () => {
    const [view, setView] = React.useState(View.Users);

    function navigate(newView: View) {
        return function (event: React.MouseEvent) {
            event.preventDefault();
            setView(newView);
        }
    }

    return <>
        <nav>
            <h3>Navigation</h3>
            <ul>
                <li><a href="#" onClick={navigate(View.Users)}>Users</a></li>
                <li><a href="#" onClick={navigate(View.Rooms)}>Rooms</a></li>
                <li><a href="#" onClick={navigate(View.Reservations)}>Reservations</a></li>
            </ul>
        </nav>
        {getPage(view)}
    </>;
}

function getPage(view: View) {
    switch (view) {
        case View.Users:
            return <UserPage />;
        case View.Rooms:
            return <RoomPage />;
        case View.Reservations:
            return <ReservationPage />;
        default:
            return null;
    }
}

enum View {
    Users,
    Rooms,
    Reservations
}