import * as React from "react";

interface Props {
    onSubmit: (email: string) => void;
}

export const UserRegisterForm: React.FC<Props> = props => {
    const [email, setEmail] = React.useState<string>("");

    function handleSubmit(event: React.FormEvent) {
        event.preventDefault();
        props.onSubmit(email);
    }

    function handleEmailChange(event: React.ChangeEvent<HTMLInputElement>) {
        setEmail(event.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            <input type="email" value={email} placeholder="Email" required onChange={handleEmailChange}/>
            <input type="submit" value="Register" />
        </form>
    );
}