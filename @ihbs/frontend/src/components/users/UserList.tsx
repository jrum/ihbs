import * as React from "react";
import { UserResponse } from "@ihbs/interfaces";

interface Props {
    users: UserResponse[];
}

export const UserList: React.FC<Props> = props => (
    <ul>
        {props.users.map(user => <li key={user.id}>{user.email}</li>)}
    </ul>
);
