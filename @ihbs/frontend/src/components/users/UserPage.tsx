import { UserResponse } from "@ihbs/interfaces";
import { UserList } from "components/users/UserList";
import { UserRegisterForm } from "components/users/UserRegisterForm";
import * as React from "react";
import { get, post } from "util/http";

export const UserPage: React.FC = () => {
    const [users, setUsers] = React.useState<UserResponse[]>([]);
    const [fetching, setFetching] = React.useState(true);
    const [error, setError] = React.useState(false);

    React.useEffect(() => {
        async function fetchUsers() {
            try {
                setUsers(await get<UserResponse[]>("/api/users"));
                setError(false);
            } catch {
                setError(true);
            } finally {
                setFetching(false);
            }
        }

        if (fetching) {
            fetchUsers();
        }
    }, [fetching]);

    async function registerUser(email: string) {
        await post("/api/users", { email });
        setFetching(true);
    }

    let content;
    if (fetching) {
        content = <p>Loading users...</p>;
    } else if (error) {
        content = <>
            <p>Failed to load users.</p>
            <button onClick={() => setFetching(true)}>Reload</button>
        </>;
    } else {
        content = <>
            <UserList users={users} />
            <UserRegisterForm onSubmit={registerUser} />
        </>;
    }

    return <>
        <h1>Users</h1>
        {content}
    </>;
}