import { ReservationResponse, MapResponse, UserResponse, RoomResponse } from "@ihbs/interfaces";
import * as React from "react";

interface Props {
    reservations: ReservationResponse[];
    rooms: MapResponse<RoomResponse>;
    users: MapResponse<UserResponse>;
}

export const ReservationList: React.FC<Props> = props => {

    function renderRow(reservation: ReservationResponse) {
        return <tr key={reservation.id}>
            <td>{props.users[reservation.userId].email}</td>
            <td>{props.rooms[reservation.roomId].name}</td>
            <td>{new Date(reservation.starts).toLocaleString()}</td>
            <td>{new Date(reservation.ends).toLocaleString()}</td>
        </tr>;
    }

    return <table>
        <thead>
            <tr>
                <th>User</th>
                <th>Room</th>
                <th>Starts</th>
                <th>Ends</th>
            </tr>
        </thead>
        <tbody>
            {props.reservations.map(x => renderRow(x))}
        </tbody>
    </table>;
}