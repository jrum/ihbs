import { ReservationResponse, RoomResponse, MapResponse, UserResponse } from "@ihbs/interfaces";
import { ReservationList } from "components/reservations/ReservationList";
import * as React from "react";
import { get } from "util/http";

export const ReservationPage: React.FC = () => {
    const [reservations, setReservations] = React.useState<ReservationResponse[]>([]);
    const [rooms, setRooms] = React.useState<MapResponse<RoomResponse>>({});
    const [users, setUsers] = React.useState<MapResponse<UserResponse>>({});
    const [fetching, setFetching] = React.useState(true);
    const [error, setError] = React.useState(false);

    React.useEffect(() => {
        async function fetchReservations() {
            try {
                const reservations = await get<ReservationResponse[]>("/api/reservations");

                const roomQuery = reservations.map(x => `ids[]=${x.roomId}`).join("&");
                const rooms = await get<MapResponse<RoomResponse>>(`/api/rooms?${roomQuery}`);

                const userQuery = reservations.map(x => `ids[]=${x.userId}`).join("&");
                const users = await get<MapResponse<UserResponse>>(`/api/users?${userQuery}`);

                setReservations(reservations);
                setRooms(rooms);
                setUsers(users);
                setError(false);
            } catch {
                setError(true);
            } finally {
                setFetching(false);
            }
        }

        if (fetching) {
            fetchReservations();
        }
    }, [fetching]);

    let content;
    if (fetching) {
        content = <p>Loading reservations...</p>;
    } else if (error) {
        content = <>
            <p>Failed to load reservations.</p>
            <button onClick={() => setFetching(true)}>Reload</button>
        </>;
    } else {
        content = <>
            <ReservationList reservations={reservations} rooms={rooms} users={users} />
        </>;
    }

    return <>
        <h1>Reservations</h1>
        {content}
    </>;
}