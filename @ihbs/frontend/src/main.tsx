import { App } from "components/App";
import * as React from "react";
import * as ReactDOM from "react-dom";
import "styles/app.scss";

ReactDOM.render(
    <App />,
    document.getElementById("app")
);
