/**
 * Helper function to GET typed JSON data
 */
export async function get<T = unknown>(url: string, init?: RequestInit) {
    const res = await fetch(url, {
        ...init,
        method: "GET",
        headers: {
            ...init?.headers,
            "Accept": "application/json"
        }
    });

    if (res.ok) {
        return await res.json() as T;
    }

    throw new Error(res.statusText);
}

/**
 * Helper function for POSTing JSON data
 */
export async function post<T = unknown>(url: string, body: any, init?: RequestInit) {
    const res = await fetch(url, {
        ...init,
        method: "POST",
        body: JSON.stringify(body),
        headers: {
            ...init?.headers,
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    });

    if (res.ok) {
        return await res.json() as T;
    }

    throw new Error(res.statusText);
}