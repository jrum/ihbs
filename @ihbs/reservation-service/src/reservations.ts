import { runInDatabase } from "@ihbs/lib";
import { v4 as uuid } from "uuid";
import { ReservationRecord, CreateReservationRecord } from "./ReservationRecord";

export function getReservation(id: string) {
    return runInDatabase(async conn => {
        const result = await conn.query<ReservationRecord>('SELECT * FROM reservations WHERE "id"=$1', [id]);
        if (result.rowCount === 1) {
            return result.rows[0];
        }
        return null;
    });
}

interface ReservationFilter {
    userId?: string;
    roomId?: string;
    after?: string;
    before?: string;
}

export function getReservations(filters: ReservationFilter) {
    return runInDatabase(async conn => {
        const conditions: string[] = [];
        const params: string[] = [];
        let paramIdx = 1;

        if (filters.userId) {
            conditions.push(`"userId" = $${paramIdx++}`);
            params.push(filters.userId);
        }
        if (filters.roomId) {
            conditions.push(`"roomId" = $${paramIdx++}`);
            params.push(filters.roomId);
        }
        if (filters.after) {
            const idx = paramIdx++;
            conditions.push(`("starts" > $${idx} OR "ends" > $${idx})`);
            params.push(filters.after);
        }
        if (filters.before) {
            const idx = paramIdx++;
            conditions.push(`("starts" < $${idx} OR "ends" < $${idx})`);
            params.push(filters.before);
        }

        const where = conditions.length > 0 ? `WHERE ${conditions.join(" AND ")}` : "";
        const result = await conn.query<ReservationRecord>(`SELECT * FROM reservations ${where}`, params);
        return result.rows;
    });
}

export function createReservation(res: CreateReservationRecord) {
    return runInDatabase(async conn => {
        const id = uuid();
        const params = [id, res.userId, res.roomId, res.starts, res.ends];
        await conn.query('INSERT INTO reservations ("id", "userId", "roomId", "starts", "ends") VALUES ($1, $2, $3, $4, $5)', params);
        return { id };
    });
}

export function deleteReservation(id: string) {
    return runInDatabase(async conn => {
        await conn.query('DELETE FROM reservations WHERE "id"=$1', [id]);
    });
}