export interface ReservationRecord {
    id: string;
    userId: string;
    roomId: string;
    starts: Date;
    ends: Date;
}

export type CreateReservationRecord = Omit<ReservationRecord, "id">;
