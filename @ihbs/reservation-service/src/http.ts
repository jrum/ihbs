import * as express from "express";
import * as asyncHandler from "express-async-handler";
import { getReservation, getReservations } from "./reservations";

export function defineHttpApi() {
    const app = express();

    app.get("/", asyncHandler(async (req, res) => {
        res.send(await getReservations(req.query));
    }));

    app.get("/:reservationId", asyncHandler(async (req, res) => {
        const reservation = await getReservation(req.params.reservationId);
        if (reservation) {
            res.send(reservation);
        } else {
            res.status(404).send();
        }
    }));

    app.post("/", express.json(), asyncHandler(async (req, res) => {
        // TODO init saga
    }));

    app.listen(80);
}
