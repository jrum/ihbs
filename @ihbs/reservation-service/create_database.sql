create database service;
\c service
drop table if exists reservations;
create table reservations (
    "id" uuid primary key,
    "userId" uuid not null,
    "roomId" uuid not null,
    "starts" timestamp with time zone not null,
    "ends" timestamp with time zone not null
);
create index on reservations ("userId");
create index on reservations ("roomId");
create index on reservations ("starts", "ends");
\q
