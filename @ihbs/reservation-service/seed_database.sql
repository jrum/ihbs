\c service
delete from reservations;
insert into reservations ("id", "userId", "roomId", "starts", "ends") values
	('f1080992-6026-49c2-bb5a-335ec703c436', '88a75c7f-7cd3-4835-9170-f2a3fb36fb69', '024f2f3a-c088-4aea-994a-da69976561d8', '2020-01-01T00:00:00Z', '2020-01-07T23:59:59.999Z'),
	('a73f41ab-6fab-45a8-a428-eae3478c5c8b', '60853f9f-8088-4200-92e4-828de5d19ab0', '024f2f3a-c088-4aea-994a-da69976561d8', '2020-01-10T07:00:00Z', '2020-01-17T18:00:00Z'),
	('720b9190-6f33-4ffc-b550-03bea8731edd', 'e46713ec-2a58-4fec-b48b-fb5503e82e8e', '02135543-4d75-4f0b-9d02-299a394683a5', '2020-01-05T10:00:00Z', '2020-01-06T16:30:00Z'),
	('2f93884c-62f8-4e8e-99d8-f24be0a32f28', 'e46713ec-2a58-4fec-b48b-fb5503e82e8e', '867baf8c-aa5f-499b-bbcb-7efb2f82ddf3', '2020-01-16T09:00:00Z', '2020-01-18T17:30:00Z');
\q