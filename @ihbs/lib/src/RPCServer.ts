import { Channel, Connection } from "amqplib";
import { RPCRequest } from "./RPCRequest";
import { RPCResponse } from "./RPCResponse";

/**
 * Used to listen for RPC requests and notification from the message queue and reply to them.
 */
export class RPCServer {

    private channel: Promise<Channel>;

    public constructor(connection: Connection) {
        this.channel = connection.createChannel();
    }

    /**
     * Starts listening for RPC requests and notifications
     * @param queue The queue to listen
     * @param onMessage Called with the message object when an RPC is received.
     */
    public async listen(queue: string, onMessage: (method: string, params?: any) => Promise<any>) {
        const channel = await this.channel;
        await channel.assertQueue(queue);

        channel.consume(queue, async message => {
            if (!message) {
                return;
            }

            let success = false;
            try {
                const rpcRequest = JSON.parse(message.content.toString()) as RPCRequest;
                const result = await onMessage(rpcRequest.method, rpcRequest.params);

                // Send a reply, if it was requested
                if (message.properties.replyTo) {
                    const rpcReply: RPCResponse = { type: "success", result };
                    const replyBuffer = Buffer.from(JSON.stringify(rpcReply));
                    success = channel.sendToQueue(message.properties.replyTo, replyBuffer, {
                        correlationId: message.properties.correlationId
                    });
                }
            } catch (error) {
                // Error while calling onMessage, build an error response and send.
                const rpcReply: RPCResponse = { type: "error", message: error.message };
                const replyBuffer = Buffer.from(JSON.stringify(rpcReply));
                success = channel.sendToQueue(message.properties.replyTo, replyBuffer, {
                    correlationId: message.properties.correlationId
                });
            } finally {
                // Success indicates that the reply was queued correctly, not that the RPC succeeded without errors.
                // If we didn't succeed sending a reply at all, nack the message, otherwise ack.
                if (success) {
                    channel.ack(message);
                } else {
                    channel.nack(message);
                }
            }
        })
    }
}
