import { Channel, Connection, Replies } from "amqplib";
import { v4 as uuid } from "uuid";
import { delay } from "./delay";
import { RPCRequest } from "./RPCRequest";
import { RPCResponse } from "./RPCResponse";

/**
 * Used to send RPC requests to the message queue and wait for a reply.
 */
export class RPCClient {

    private initialized: Promise<{
        channel: Channel,
        consumer: Replies.Consume,
        replyQueue: string;
    }>;

    /** Contains the promise resolver functions by correlation ID for pending RPCs */
    private calls: Map<string, (message: Buffer) => void>;

    public constructor(connection: Connection) {
        this.initialized = this.init(connection);
        this.calls = new Map();
    }

    private async init(connection: Connection) {
        // Create the reply queue for this client.
        const channel = await connection.createChannel();
        const assert = await channel.assertQueue("", {
            exclusive: true,
            autoDelete: true,
            durable: false
        });
        const replyQueue = assert.queue;
        
        // Start consuming the reply queue
        const consumer = await channel.consume(replyQueue, message => {
            if (!message) {
                return;
            }

            // Try to get the resolver from the active RPC map and execute it.
            const correlationId = message.properties.correlationId;
            this.calls.get(correlationId)?.(message.content);
            this.calls.delete(correlationId);

            channel.ack(message);
        });

        return {
            channel,
            consumer,
            replyQueue
        };
    }

    /**
     * Sends an RPC request and waits for the response.
     * @param method The RPC method to call
     * @param params Params for the method
     * @param queue The queue to send the request to
     * @param timeoutMs - Optional timeout, defaults to 500 ms if not specified
     */
    public async request<T = any>(method: string, params: any, queue: string, timeoutMs = 500) {
        const { channel, replyQueue } = await this.initialized;
        await channel.assertQueue(queue);

        // Create a random correlation id and insert a new resolver to the active RPC calls map
        const correlationId = uuid();
        const reply = new Promise<Buffer>(resolve => this.calls.set(correlationId, resolve));

        // Create and send the RPC
        const rpcMessage: RPCRequest = { method, params }
        const buffer = Buffer.from(JSON.stringify(rpcMessage));
        const sent = channel.sendToQueue(queue, buffer, {
            correlationId,
            replyTo: replyQueue,
            expiration: timeoutMs
        });

        if (!sent) {
            this.calls.delete(correlationId);
            throw new Error("Failed to send message");
        }

        const timeout = delay(timeoutMs).then(() => { this.calls.delete(correlationId) });
        const replyBuffer = await Promise.race([reply, timeout]);

        // The timeout promise always returns undefined and the reply promise always returns a Buffer,
        // so we can detect which promise resolved first from the value of the replyBuffer
        if (!replyBuffer) {
            throw new Error("Timeout exceeded");
        }

        const rpcReply = JSON.parse(replyBuffer.toString()) as RPCResponse<T>;
        if (rpcReply.type === "error") {
            throw new Error(rpcReply.message);
        }

        return rpcReply.result;
    }
    
    /**
     * Sends an RPC notification.
     * @param method The RPC method to call
     * @param params Params for the method
     * @param queue The queue to send the notification to
     */
    public async notify(method: string, params: any, queue: string) {
        const { channel } = await this.initialized;
        await channel.assertQueue(queue);

        // Create and send the RPC
        const rpcMessage: RPCRequest = { method, params }
        const buffer = Buffer.from(JSON.stringify(rpcMessage));
        const sent = channel.sendToQueue(queue, buffer);

        if (!sent) {
            throw new Error("Failed to send message");
        }
    }
}
