/**
 * The format of all RPC requests and notifications.
 */
export interface RPCRequest<T = any> {
    method: string;
    params?: T;
}
