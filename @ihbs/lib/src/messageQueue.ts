import { connect, Connection } from "amqplib";
import { retry } from "./retry";

/**
 * Ensures that RabbitMQ can be connected to. Does not return an open connection.
 * Used to delay the application startup, since NodeJS starts the application
 * much faster than what it takes for RabbitMQ to begin accepting connections.
 * Throws if connection can't be established after a timeout.
 */
export async function ensureMQConnection() {
    const timeout = 15000;
    const retryDelay = 500;

    await retry(connectToMQ, timeout, retryDelay);
}

/**
 * Connects to the message queue.
 */
function connectToMQ() {
    return connect({
        hostname: process.env.RABBITMQ_HOST,
        username: process.env.RABBITMQ_USER,
        password: process.env.RABBITMQ_PASS
    });
}

let connection: Connection | undefined = undefined;

/**
 * Returns a shared connection for this process.
 * Do not close the returned connection!
 */
export async function getMQConnection() {
    if (connection) {
        return connection;
    }

    connection = await connectToMQ();
    // If we lose connection to the message broker, we are either having network issues
    // or the broker crashed. Either way, crash this process and let the underlying
    // infrastructure restart it later. It will probably fix itself after the restart.
    connection.on("close", () => process.exit(1));

    return connection;
}