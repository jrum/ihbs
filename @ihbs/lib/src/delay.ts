/**
 * Returns a promise that resolves after the timeout.
 * @param ms Timeout
 */
export function delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
