interface RPCSuccessResponse<T> {
    type: "success";
    result: T;
}

interface RPCErrorResponse {
    type: "error";
    message: string;
}

/**
 * The format of all RPC responses.
 */
export type RPCResponse<T = any> = RPCSuccessResponse<T> | RPCErrorResponse;
