import { delay } from "./delay";

/**
 * Retries an action until it succeeds or a timeout is exceeded. If the timeout is exceeded,
 * re-throws the original error the action threw.
 */
export async function retry<T>(action: () => Promise<T>, timeoutMS: number, retryDelayMS: number) {
    const timeoutPromise = delay(timeoutMS);
    
    while (true) {
        try {
            return await action();
        } catch (error) {
            if (timeoutMS <= 0) {
                throw error;
            }

            const retryPromise = delay(retryDelayMS);
            const failPromise = timeoutPromise.then(() => { throw error; });

            await Promise.race([retryPromise, failPromise]);
        }
    }
}
