import { Pool, PoolClient } from "pg";
import { retry } from "./retry";

const pool = new Pool();

/**
 * Ensures that PostgreSQL can be connected to. Does not return an open connection.
 * Used to delay the application startup, since NodeJS may start the application
 * faster than what it takes for PostgreSQL to start accepting connections.
 * Throws if connection can't be established after a timeout.
 */
export async function ensureDatabaseConnection() {
    const timeout = 10000;
    const retryDelay = 500;

    const conn = await retry(connectToDatabase, timeout, retryDelay);
    conn.release();
}

/**
 * Connects to the database.
 */
export function connectToDatabase() {
    return pool.connect();
}

/**
 * Runs a thing in the database. Safely releases the connection afterwards.
 * @param action The action to execute
 * @returns Whatever the action returned
 */
export async function runInDatabase<T>(action: (connection: PoolClient) => Promise<T>) {
    const conn = await connectToDatabase();
    try {
        return await action(conn);
    } finally {
        conn.release();
    }
}
