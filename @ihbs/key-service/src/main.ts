import { ensureDatabaseConnection, ensureMQConnection } from "@ihbs/lib";
import { defineHttpApi } from "./http";

export async function main() {
    await ensureMQConnection();
    await ensureDatabaseConnection();

    defineHttpApi();
}
