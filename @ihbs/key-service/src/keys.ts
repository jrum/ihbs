import { runInDatabase } from "@ihbs/lib";
import { v4 as uuid } from "uuid";
import { KeyRecord, CreateKeyRecord } from "./KeyRecord";

export function getKey(id: string) {
    return runInDatabase(async conn => {
        const result = await conn.query<KeyRecord>('SELECT * FROM keys WHERE "id"=$1', [id]);
        if (result.rowCount === 1) {
            return result.rows[0];
        }
        return null;
    });
}

export function getKeys() {
    return runInDatabase(async conn => {
        const result = await conn.query<KeyRecord>('SELECT * FROM keys');
        return result.rows;
    })
}

export function createKey(key: CreateKeyRecord) {
    return runInDatabase(async conn => {
        const id = uuid();
        const params = [id, key.roomId, key.code, key.validFrom, key.validUntil];
        await conn.query('INSERT INTO keys ("id", "roomId", "code", "validFrom", "validUntil) VALUES ($1, $2, $3, $4, $5)', params);
        return { id };
    });
}

export function deleteKey(id: string) {
    return runInDatabase(async conn => {
        await conn.query('DELETE FROM keys WHERE "id"=$1', [id]);
    });
}

/**
 * A random 5-digit key code
 */
function generateKeyCode() {
    return [0, 0, 0, 0, 0]
        .map(() => Math.floor(Math.random() * 10))
        .join("");
}