import { RPCServer, getMQConnection } from "@ihbs/lib";

export async function defineMqApi() {
    const connection = await getMQConnection();
    const server = new RPCServer(connection);
    server.listen("keys", messageReceived);
}

async function messageReceived(method: string, params?: any) {
    switch (method) {
        default:
            throw new Error("Unknown method");
    }
}