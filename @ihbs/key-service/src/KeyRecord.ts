export interface KeyRecord {
    id: string;
    roomId: string;
    code: string;
    validFrom: Date;
    validUntil: Date;
}

export type CreateKeyRecord = Omit<KeyRecord, "id">;