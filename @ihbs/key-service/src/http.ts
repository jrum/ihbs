import * as express from "express";
import * as asyncHandler from "express-async-handler";
import { getKey, getKeys } from "./keys";

export function defineHttpApi() {
    const app = express();

    app.get("/", asyncHandler(async (req, res) => {
        res.send(await getKeys());
    }));

    app.get("/:keyId", asyncHandler(async (req, res) => {
        const key = await getKey(req.params.keyId);
        if (key) {
            res.send(key);
        } else {
            res.status(404).send();
        }
    }));

    app.listen(80);
}