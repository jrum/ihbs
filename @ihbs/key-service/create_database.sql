create database service;
\c service
drop table if exists keys;
create table keys (
    "id" uuid primary key,
    "roomId" uuid not null,
    "code" text not null,
    "validFrom" timestamp with time zone not null,
    "validUntil" timestamp with time zone not null
);
create index on keys ("roomId", "code", "validFrom", "validUntil");
\q
