create database service;
\c service
drop table if exists users;
create table users (
	"id" uuid primary key,
	"email" text not null
);
create unique index on users ("email");
\q
