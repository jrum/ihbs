\c service
delete from users;
insert into users ("id", "email") values
	('88a75c7f-7cd3-4835-9170-f2a3fb36fb69', 'john.johnson@example.com'),
	('60853f9f-8088-4200-92e4-828de5d19ab0', 'keith.allen@example.com'),
	('a932baa0-832e-4d3f-a075-9b1a23ea73c8', 'debra.ross@example.com'),
	('e46713ec-2a58-4fec-b48b-fb5503e82e8e', 'diana.hill@example.com'),
	('f0e28aed-52b8-4945-bbad-87290860f615', 'eric.cook@example.com');
\q