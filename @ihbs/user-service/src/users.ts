import { runInDatabase } from "@ihbs/lib";
import { v4 as uuid } from "uuid";
import { UserRecord, CreateUserRecord } from "./UserRecord";

export function getUser(id: string) {
    return runInDatabase(async conn => {
        const result = await conn.query<UserRecord>('SELECT * FROM users WHERE "id"=$1', [id]);
        if (result.rowCount === 1) {
            return result.rows[0];
        }
        return null;
    });
}

export function getUsers(ids?: string[]) {
    return runInDatabase(async conn => {
        let result;
        if (!ids) {
            result = await conn.query<UserRecord>('SELECT * FROM users');
        } else {
            // Build the "$1,$2,...$n" string
            const paramIdxList = ids.map((_, i) => `$${i + 1}`).join(",");
            result = await conn.query<UserRecord>(`SELECT * FROM users WHERE "id" IN (${paramIdxList})`, ids);
        }
        return result.rows;
    });
}

export function createUser(user: CreateUserRecord) {
    return runInDatabase(async conn => {
        const id = uuid();
        const params = [id, user.email];
        await conn.query('INSERT INTO users ("id", "email") VALUES ($1, $2)', params);
        return { id };
    });
}
