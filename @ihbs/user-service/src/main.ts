import { ensureDatabaseConnection, ensureMQConnection } from "@ihbs/lib";
import { defineHttpApi } from "./http";
import { defineMqApi } from "./mq";

export async function main() {
    await ensureMQConnection();
    await ensureDatabaseConnection();

    defineHttpApi();
    defineMqApi();
}
