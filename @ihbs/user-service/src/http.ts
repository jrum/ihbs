import { MapResponse } from "@ihbs/interfaces";
import * as express from "express";
import * as asyncHandler from "express-async-handler";
import { UserRecord } from "./UserRecord";
import { createUser, getUser, getUsers } from "./users";

export function defineHttpApi() {
    const app = express();

    app.get("/", asyncHandler(async (req, res) => {
        const ids = req.query.ids as string[];
        const users = await getUsers(ids);

        // If the user doesn't specify user ids, return the full list of users as an array.
        if (!ids) {
            res.send(users);
        }
        // If the user specified a list of ids, return the filtered users as a map.
        else {
            const map = users.reduce<MapResponse<UserRecord>>(
                (prev, curr) => ({
                    ...prev,
                    [curr.id]: curr
                }),
                {}
            );
            res.send(map);
        }
    }));

    app.get("/:userId", asyncHandler(async (req, res) => {
        const user = await getUser(req.params.userId);
        if (user) {
            res.send(user);
        } else {
            res.status(404).send();
        }
    }));

    app.post("/", express.json(), asyncHandler(async (req, res) => {
        res.status(201).send(await createUser(req.body));
    }));

    app.listen(80);
}