export interface UserRecord {
    id: string;
    email: string;
}

export type CreateUserRecord = Omit<UserRecord, "id">;
