import { getMQConnection, RPCServer } from "@ihbs/lib";
import { getUsers } from "./users";
import { UserResponse } from "@ihbs/interfaces";

export async function defineMqApi() {
    const connection = await getMQConnection();
    const server = new RPCServer(connection);
    server.listen("users", messageReceived);
}

async function messageReceived(method: string, params?: any) {
    switch (method) {
        case "getUsers":
            if (Array.isArray(params) && params.length > 0) {
                return await getUsers(params) as UserResponse[];
            } else {
                return await getUsers() as UserResponse[];
            }
        default:
            throw new Error("Unknown method");
    }
}