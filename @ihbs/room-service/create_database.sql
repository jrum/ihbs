create database service;
\c service
drop table if exists rooms;
create table rooms (
    "id" uuid primary key,
    "name" text not null,
    "price" double precision not null
);
\q
