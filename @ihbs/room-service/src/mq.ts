import { RPCServer, getMQConnection } from "@ihbs/lib";
import { getRooms } from "./rooms";
import { RoomResponse } from "@ihbs/interfaces";

export async function defineMqApi() {
    const connection = await getMQConnection();
    const server = new RPCServer(connection);
    server.listen("rooms", messageReceived);
}

async function messageReceived(method: string, params?: any) {
    switch (method) {
        case "getRooms":
            if (Array.isArray(params) && params.length > 0) {
                return await getRooms(params) as RoomResponse[];
            } else {
                return await getRooms() as RoomResponse[];
            }
        default:
            throw new Error("Unknown method");
    }
}