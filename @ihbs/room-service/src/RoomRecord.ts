export interface RoomRecord {
    id: string;
    name: string;
    price: number;
}

export type CreateRoomRecord = Omit<RoomRecord, "id">;
