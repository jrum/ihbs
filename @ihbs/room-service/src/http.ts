import { MapResponse } from "@ihbs/interfaces";
import * as express from "express";
import * as asyncHandler from "express-async-handler";
import { RoomRecord } from "./RoomRecord";
import { createRoom, getRoom, getRooms } from "./rooms";

export function defineHttpApi() {
    const app = express();

    app.get("/", asyncHandler(async (req, res) => {
        const ids = req.query.ids as string[];
        const rooms = await getRooms(ids);

        // If the user doesn't specify room ids, return the full list of rooms as an array.
        if (!ids) {
            res.send(rooms);
        }
        // If the user specified a list of ids, return the filtered rooms as a map.
        else {
            const map = rooms.reduce<MapResponse<RoomRecord>>(
                (prev, curr) => ({
                    ...prev,
                    [curr.id]: curr
                }),
                {}
            );
            res.send(map);
        }
    }));

    app.get("/:roomId", asyncHandler(async (req, res) => {
        const room = await getRoom(req.params.roomId);
        if (room) {
            res.send(room);
        } else {
            res.status(404).send();
        }
    }));

    app.post("/", express.json(), asyncHandler(async (req, res) => {
        res.status(201).send(await createRoom(req.body));
    }));

    app.listen(80);
}