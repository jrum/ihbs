import { runInDatabase } from "@ihbs/lib";
import { v4 as uuid } from "uuid";
import { RoomRecord, CreateRoomRecord } from "./RoomRecord";

export function getRoom(id: string) {
    return runInDatabase(async conn => {
        const result = await conn.query<RoomRecord>('SELECT * FROM rooms WHERE "id"=$1', [id]);
        if (result.rowCount === 1) {
            return result.rows[0];
        }
        return null;
    });
}

export function getRooms(ids?: string[]) {
    return runInDatabase(async conn => {
        let result;
        if (!ids) {
            result = await conn.query<RoomRecord>('SELECT * FROM rooms');
        } else {
            // Build the "$1,$2,...$n" string
            const paramIdxList = ids.map((_, i) => `$${i + 1}`).join(",");
            result = await conn.query<RoomRecord>(`SELECT * FROM rooms WHERE "id" IN (${paramIdxList})`, ids);
        }
        return result.rows;
    });
}

export function createRoom(room: CreateRoomRecord) {
    return runInDatabase(async conn => {
        const id = uuid();
        const params = [id, room.name, room.price];
        await conn.query('INSERT INTO rooms ("id", "name", "price") VALUES ($1, $2, $3)', params);
        return { id };
    });
}

export function deleteRoom(id: string) {
    return runInDatabase(async conn => {
        await conn.query('DELETE FROM rooms WHERE "id"=$1', [id]);
    });
}