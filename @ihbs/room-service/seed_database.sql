\c service
delete from rooms;
insert into rooms ("id", "name", "price") values
	('024f2f3a-c088-4aea-994a-da69976561d8', '101', 5.00),
	('f62b06b7-41e4-479d-88d4-01ec598c5a34', '102', 6.25),
	('d7e80e89-42d6-4a90-8843-bcdd089bdab5', '103', 4.95),
	('867baf8c-aa5f-499b-bbcb-7efb2f82ddf3', '201', 10.50),
	('02135543-4d75-4f0b-9d02-299a394683a5', '202', 17.80),
	('0fae596a-2314-49cc-8173-beee870970b1', '203', 29.95);
\q